<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);


$id = $_GET['id'];
$query = "SELECT * FROM ads WHERE id=$id";  

$result = $mysqli->query($query);
?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">					
                    <?php if(isset($result) && $result->num_rows>0){ ?>
                    <?php if(isset($location)){ ?>
                    <h2>All Ads found in <?php echo $location; ?></h2>
                    <?php } ?>
                    <div class="row">
                    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)){ ?>
                        <div class="col-sm-6">
                        <img class="img-responsive" src="admin/upload/ads/<?php echo $row['photo']; ?>" />
                        </div>    
                        <div class="col-sm-6 ads_details">
                           <h2 class="title_details"><?php echo $row['title']; ?></h2> 
                           <p><b>Price:</b>BDT <?php echo $row['price']; ?></p>    
                           <p><b>Price Type:</b> <?php echo $row['price_type']; ?></p>    
                           <p><b>Type:</b><?php echo $row['type']; ?></p>    
                           <p><b>Mileage:</b><?php echo $row['mileage']; ?> KM</p>    
                           <p><b>Contact:</b><?php echo $row['phone']; ?></p>    
                           <p><b>Location:</b><?php echo $row['location']; ?></p>    
                        </div> 
                        <div class="col-sm-12">
                            <p><?php echo $row['description']; ?></p>
                        </div>
                    <?php } ?>    
                  
                    </div>   
                    
                <?php } else { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="text-danger">No Ads Found!</h3>
                        </div>
                    </div>
                <?php } ?>    
			  </div>
			</div>
			</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

