<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_POST['full_name']))
{
    $full_name = $_POST['full_name'];
    $email = $_POST['email'];
    $password = sha1($_POST['password']);
    $phone = $_POST['phone'];
    
    $query = "INSERT INTO vehicle_owner VALUES(null,'$full_name','$email','$password','$phone')";
    
    $mysqli->query($query);
    $success = "Your account has been created!";
    
}

?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
				<div class="col span_2_of_3">
				  <div class="contact-form">
                      <?php if(isset($success)){ ?>
                      <label class="text-success">
                      <?php echo $success; ?>
                      </label>
                      <?php } ?>
				  	<h3>Signup As Owner</h3>
				<form method="post" action="">
                    <div class="row">
					   <div class="col-sm-6">
						    	<span><label>Full Name</label></span>
						    	<span><input name="full_name" type="text" class="textbox" required></span>
				        </div>
				        <div class="col-sm-6">
						    	<span><label>Email</label></span>
						    	<span><input name="email" type="email" class="textbox" required></span>
						  </div>
						  <div class="col-sm-6">
						     	<span><label>Password</label></span>
						    	<span><input name="password" type="password" class="textbox" required></span>
						    </div>
                        <div class="col-sm-6">
						     	<span><label>Phone</label></span>
						    	<span><input name="phone" type="text" class="textbox" required></span>
						    </div>
                            </div>
                            <div class="col-sm-3 ">
						   <div>
						   		<span><input type="submit" value="Signup"></span>
						  </div>
                            </div>
					    </form>

				    </div>
  				</div>	
			  </div>
			</div>
			</div>
		 	<!---End-contact----->
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

