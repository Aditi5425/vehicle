<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_GET['del']))
{
    $ads_id = $_GET['del'];
    $query = "DELETE FROM ads WHERE id=$ads_id";
    $result = $mysqli->query($query);
    $success = 'Ads Deleted';
}

$owner_id = $_SESSION['owner_id'];
$query = "SELECT * FROM ads WHERE owner_id=$owner_id";
$result = $mysqli->query($query);

$query_expire = "SELECT * FROM registration_info WHERE owner_id=$owner_id and (expire_date - DATE(NOW())) < 10";
$result_expire = $mysqli->query($query_expire);
?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
                    <?php if(isset($result)){ ?>
                    <div class="col span_3_of_3">
                    
                        <?php if($result_expire->num_rows>0) { ?>
                        <marquee>
                        <?php while($r1 = $result_expire->fetch_array(MYSQLI_ASSOC)){ ?>
                        <label class="text-danger">
                        The vehicle with registration id# <?php echo $r1['registration_number']; ?> will expire soon.</label> 
                        <?php } ?>
                            </marquee>
                        <?php } ?>
                        
                    <h2>My Ads</h2>
                    <a href="new-ads.php" class="btn btn-primary">Create New Ads</a>
                    <?php if($result->num_rows>0){ ?>    
                    <table class="table table-bordered">
                    <tr><th>Title</th><th>Description</th><th>Price</th><th>Price Type</th><th>Mileage</th><th>Photo</th>
                    <th>Delete</th>    
                        </tr>
                    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)){ ?>    
                    <tr>
                        <td><?php echo $row['title']; ?></td>
                        <td><?php echo $row['description']; ?></td>
                        <td><?php echo $row['price']; ?> BDT</td>
                        <td><?php echo $row['price_type']; ?></td>
                        <td><?php echo $row['mileage']; ?>KM</td>
                        <td><img src="admin/upload/ads/<?php echo $row['photo']; ?>" /> </td>
                        <td><a class="text-danger"  href="my-ads.php?del=<?php echo $row['id']; ?>">Delete</a></td>
                    </tr>
                        <?php } ?>
                    </table>
                    <?php } else { ?>
                        <br/>
                        <br/>
                        <label class="text-danger">No Ads Found.</label>
                    <?php } ?>
                </div>
                <?php } ?>
			  </div>
			</div>
			</div>
		 	<!---End-contact----->
		 	<div class="bottom-grids">
		 		<div class="wrap">
		 		<div class="social-links">
		 			<ul>
		 				<li><a href="#">Facebook</a></li>
		 				<li><a href="#">Twitter</a></li>
		 				<li><a href="#">Google+</a></li>
		 				<li><a href="#">Linkedin</a></li>
		 			</ul>
		 		</div>
		 		<div class="mobile-no">
		 			 <span>+91 123 456789</span>
		 		</div>
		 		<div class="footer-logo">
		 			<a href="#">CITY TAXI</a>
		 		</div>
		 		<div class="copy-right">
		 			<p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		 		</div>
		 		<div class="top-to-page">
						<a href="#top" class="scroll"> </a>
						<div class="clear"> </div>
					</div>
		 	</div>
		 	</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

