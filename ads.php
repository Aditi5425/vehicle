<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_GET['location']))
{
  $location = $_GET['location'];
  $query = "SELECT * FROM ads WHERE location='$location'";    
}
else{
  $query = "SELECT * FROM ads";  
}
$result = $mysqli->query($query);
?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
				<div class="row">
				  <div class="contact-form">
				  	<h3></h3>
					    <form method="get" action="">
                            <div class="col-sm-4">
                            <div >
						    	<span><label>Search in</label></span>
						    	<span>
                        <select name="location" id="search_by">
            <option value="Dhaka">Dhaka</option>
            <option <?php if(isset($location) && $location=='Chittagong') echo "selected='selected'"; ?> value="Chittagong">Chittagong</option>
            <option <?php if(isset($location) && $location=='Rajshahi') echo "selected='selected'"; ?> value="Rajshahi">Rajshahi</option>
            <option <?php if(isset($location) && $location=='Khulna') echo "selected='selected'"; ?> value="Khulna">Khulna</option>
            <option <?php if(isset($location) && $location=='Sylhet') echo "selected='selected'"; ?> value="Sylhet">Sylhet</option>
            <option <?php if(isset($location) && $location=='Rangpur') echo "selected='selected'"; ?> value="Rangpur">Rangpur</option>
            <option <?php if(isset($location) && $location=='Barisal') echo "selected='selected'"; ?> value="Barisal">Barisal</option>
                                </select>
                                </span>
						    </div>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
						   <div>
						   		<span><input type="submit" value="Search"></span>
						  </div>
                            </div>
					    </form>

				    </div>
  				</div>	
                    <?php if(isset($result) && $result->num_rows>0){ ?>
                    <?php if(isset($location)){ ?>
                    <h2>All Ads found in <?php echo $location; ?></h2>
                    <?php } ?>
                    <div class="row">
                    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)){ ?>     
                      <div class="col-sm-4 col-md-4">
                        <div class="thumbnail">
                          <img class="ads_image" src="admin/upload/ads/<?php echo $row['photo'];?>" alt="...">
                          <div class="caption">
                            <h3 class="ads_title">
                            <a href="ads-details.php?id=<?php echo $row['id']; ?>"><?php echo $row['title']; ?></a></h3>
                            <h4 class="ads_price">BDT <?php echo $row['price']; ?></h4>
                            <p class="text-center">Location: <?php echo $row['location']; ?></p>
                          </div>
                        </div>
                        </div>
                    <?php } ?>    
                  
                    </div>   
                    
                <?php } else { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="text-danger">No Ads Found!</h3>
                        </div>
                    </div>
                <?php } ?>    
			  </div>
			</div>
			</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

