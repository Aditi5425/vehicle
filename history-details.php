<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

$registration_number =$_GET['registration_number'];

if(isset($_GET['del']))
{
   $id =  $_GET['del'];
   $query = "DELETE FROM history WHERE id=$id";
   $result = $mysqli->query($query);
   $success = "Deleted!";
}
if(isset($_POST['history_date']))
{
    $history_date = $_POST['history_date'];
    $history_details = $_POST['history_details'];
    
    $query = "INSERT into history VALUES(null,$registration_number,'$history_date','$history_details')";
    $result = $mysqli->query($query);
    $success= "History Added Successfully!";  
}


$owner_id = $_SESSION['owner_id'];

$query = "SELECT * FROM history WHERE registration_number=$registration_number ORDER BY history_date DESC";
$result = $mysqli->query($query);
?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
                    <?php if(isset($result)){ ?>
                    <div class="col span_3_of_3">
                    <h2>All History for Vehicle #<?php echo $registration_number; ?></h2>
                        <?php if(isset($success)){ ?>
                      <label class="text-success">
                      <?php echo $success; ?>
                      </label>
                      <?php } ?>
                        <br/>
                    <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Add New History</button>
                        <br/>
                    <?php if($result->num_rows>0) { ?>    
                    <table class="table table-bordered">
                    <tr>
                        <th>Registration Number</th><th>History Date</th>
                    <th>History Details</th>    
                    <th>Delete</th>    
                        </tr>
                    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)){ ?>    
                    <tr>
                        <td><?php echo $row['registration_number']; ?></td>
                
                        <td><?php echo $row['history_date']; ?></td>
                        <td><?php echo $row['history_details']; ?></td>
                        <td>
                            <a class="btn btn-danger" href="history-details.php?del=<?php echo $row['id']; ?>&registration_number=<?php echo $row['registration_number']; ?>">Delete</a>
                        </td>
                    </tr>
                        <?php } ?>
                    </table>
                    <?php }else{ ?>
                    <label class="text-danger">There is no history for this vehicle</label> 
                    <?php } ?>
                </div>
                <?php } ?>
			  </div>
			</div>
			</div>
		 	<!---End-contact----->
		 	<div class="bottom-grids">
		 		<div class="wrap">
		 		<div class="social-links">
		 			<ul>
		 				<li><a href="#">Facebook</a></li>
		 				<li><a href="#">Twitter</a></li>
		 				<li><a href="#">Google+</a></li>
		 				<li><a href="#">Linkedin</a></li>
		 			</ul>
		 		</div>
		 		<div class="mobile-no">
		 			 <span>+91 123 456789</span>
		 		</div>
		 		<div class="footer-logo">
		 			<a href="#">CITY TAXI</a>
		 		</div>
		 		<div class="copy-right">
		 			<p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		 		</div>
		 		<div class="top-to-page">
						<a href="#top" class="scroll"> </a>
						<div class="clear"> </div>
					</div>
		 	</div>
		 	</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add New History</h4>
      </div>
      <div class="modal-body">
       <form method="post">
      <div class="form-group">
        <label for="exampleInputEmail1">History date</label>
        <input name="history_date" type="text" class="form-control" id="exampleInputEmail1" placeholder="YYYY-MM-DD">
      </div>
        <div class="form-group">
        <label for="exampleInputEmail1">History Details</label>
        <textarea name="history_details" class="form-control"></textarea>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
          <script src="js/jquery-1.12.3.js"></script>   
      <script src="js/bootstrap.min.js"></script> 
     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

