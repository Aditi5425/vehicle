<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_POST['search_by']))
{
    if($_POST['search_by']=='registration_number')
    {
        $val = $_POST['registration_number'];
        $query = "SELECT * FROM registration_info WHERE registration_number=$val";
    }
    else if($_POST['search_by']=='registration_date')
    {
        $val = $_POST['registration_date'];
        $query = "SELECT * FROM registration_info WHERE registration_date='$val'";
    }
    else if($_POST['search_by']=='owner_id')
    {
        $val = $_POST['owner_id'];
        $query = "SELECT * FROM registration_info WHERE owner_id=$val";
    }
    else
    {
        $error = "Please select something";
    }
    
    if(!isset($error))
    $result = $mysqli->query($query);
       
}

?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
				<div class="col span_2_of_3">
				  <div class="contact-form">
				  	<h3>Search Vehicle</h3>
					    <form method="post" action="">
                            <div class="col-sm-4">
                            <div >
						    	<span><label>Search By</label></span>
						    	<span>
                                <select name="search_by" id="search_by">
                                    <option value="0">Select</option>
        <option value="registration_number">Registration Number</option>
        <option value="registration_date">Registration Date</option>
        <option value="owner_id">Owner ID</option>
                                </select>
                                </span>
						    </div>
                            </div>
                            <div class="col-sm-4">
					    	<div class="hidden_normally" id="registration_number">
						    	<span><label>Registration Number</label></span>
						    	<span><input name="registration_number" type="number" class="textbox"></span>
						    </div>
						    <div class="hidden_normally" id="registration_date">
						    	<span><label>Registration Date</label></span>
						    	<span><input name="registration_date" type="text" class="textbox"></span>
						    </div>
						    <div class="hidden_normally" id="owner_id">
						     	<span><label>Owner ID</label></span>
						    	<span><input name="owner_id" type="number" class="textbox"></span>
						    </div>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
						   <div>
						   		<span><input type="submit" value="Search"></span>
						  </div>
                            </div>
					    </form>

				    </div>
  				</div>	
                    <?php if(isset($result)){ ?>
                    <div class="col span_3_of_3">
                    <h3>Search Results</h3>
                    <table class="table table-bordered">
                    <tr><th>Registration Number</th><th>Type</th><th>Category</th><th>Registration Date</th><th>Expire Date</th></tr>
                    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)){ ?>    
                    <tr>
                        <td><?php echo $row['registration_number']; ?></td>
                        <td><?php echo $row['type']; ?></td>
                        <td><?php echo $row['category']; ?></td>
                        <td><?php echo $row['registration_date']; ?></td>
                        <td><?php echo $row['expire_date']; ?></td>
                    </tr>
                        <?php } ?>
                    </table>
                    
                </div>
                <?php } ?>
			  </div>
			</div>
			</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

