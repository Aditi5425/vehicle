<?php include("header.php"); ?>
<?php
function AddDays($current_date,$days_to_add)
{
    $date = new DateTime($current_date);
    $days = "P".$days_to_add."D";
    $date->add(new DateInterval($days));

    return $date->format('Y-m-d');
}

if(isset($_POST['license_number']))
{   
    // Taking Login Form Data
    $license_number = $_POST['license_number'];
    $type = $_POST['type'];
    $full_name = $_POST['full_name'];
    $dob = $_POST['dob'];
    $national_id = $_POST['national_id'];
    $registration_date = $_POST['registration_date'];
    $validation_period = $_POST['validation_period'];
    
    $expire_date = AddDays($registration_date,365*$validation_period);
    
    
    $photo = basename($_FILES["photo"]["name"]);    $target_file = "upload/". $photo; 
     
    // Connecting Database
    $mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);
    
    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file))
    {

        // Running Query
        $result = $mysqli->query("INSERT INTO driving_license VALUES(null,'$full_name','$dob','$national_id','$license_number','$type','$registration_date','$expire_date','$photo',$validation_period)");
        $success = "saved Successfully!";            
    }
    else
    {
        $error = "There is a problem with uploading";
    }
    
}

    // Connecting Database
    $mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

    // Running Query
    $result = $mysqli->query("SELECT * FROM driving_license");
?>
           
        <div class="row">
            <h2>All Driving License</h2>
            <?php if(isset($success)){ ?>
            <label class="label-success"><?php echo $success; ?></label>
            <?php } ?>
            <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Add New</button>
            <div class="col-sm-12">
                <table class="table table-bordered">
                    <tr>
                        <th>License Number</th>
                        <th>Status</th>
                        <th>Name</th>
                        <th>Photo</th>
                        <th>DOB</th>
                        <th>Type</th>
                        <th>National ID</th>
                        <th>Registration Date</th>
                        <th>Expire Date</th>
                        <th>Action</th>                       
                    </tr>
                    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)){ ?>
                    <tr>
                        <td><?php echo $row['license_number']; ?></td>
                        <td><?php echo $row['status']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><img width="50" height="50" src="upload/driver/<?php echo $row['photo']; ?>" /></td>
                        <td><?php echo $row['dob']; ?></td>
                        <td><?php echo $row['type']; ?></td>
                        <td><?php echo $row['national_id']; ?></td>
                        <td><?php echo $row['registration_date']; ?></td>
                        <td><?php echo $row['expire_date']; ?></td>
                        <td><a href="driving-license-edit.php?id=<?php echo $row['id']; ?>" class="btn btn-primary">Edit</a> <br/><br/>
                        <a href="add-history.php" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New License </h4>
      </div>
      <div class="modal-body">
        <form method="post" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleInputEmail1">License Number</label>
            <input name="license_number" type="text" class="form-control" id="exampleInputEmail1" placeholder="License Number">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Full Name</label>
            <input name="full_name" type="text" class="form-control" id="exampleInputPassword1" placeholder="Full Name">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"> Date of Birth</label>
            <input name="dob" type="text" class="form-control" id="exampleInputPassword1" placeholder="Ex: YYYY-MM-DD">
          </div>
            <div class="form-group">
            <label for="exampleInputPassword1">National ID</label>
            <input name="national_id" type="text" class="form-control" id="exampleInputPassword1" placeholder="National ID">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Registration Date</label>
            <input name="registration_date" type="text" class="form-control" id="exampleInputPassword1" placeholder="Ex: YYYY-MM-DD">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Photo</label>
            <input name="photo" type="file" id="exampleInputFile">
            <p class="help-block">Example block-level help text here.</p>
          </div>
          <div class="form-group">
                <label>Validation Period</label>
                <select name="validation_period" class="form-control">
                    <option value="5">5 Years</option>  
                    <option value="10">10 Years</option>  
                    <option value="3">3 Years</option>  
                    <option value="15">15 Years</option>  
                </select>
          </div>
          <div class="form-group">
                <label>Type</label>
                <select name="type" class="form-control">
                    <option value="light">Light</option>  
                    <option value="medium">Medium</option>  
                    <option value="heavy">Heavy</option>  
                </select>
            </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    </div>
      <script src="js/jquery-1.12.3.js"></script>   
      <script src="js/bootstrap.min.js"></script>   
</body>
</html>