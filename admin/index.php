<?php include("header.php"); ?>
<?php
    
    // Connecting Database
    $mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

 
if(isset($_GET['approve']))
{
    $id = $_GET['approve'];
    $query = "UPDATE registration_info SET status=1 WHERE id=$id";
    $mysqli->query($query);
}

    // Running Query
    $result = $mysqli->query("SELECT * FROM registration_info");

?>
        
        <div class="row">
            <h2>All Vehicles</h2>
            <div class="col-sm-12">
                <table class="table table-bordered">
                    <tr>
                        <th>Registration Number</th>
                        <th>Type</th>
                        <th>Category</th>
                        <th>Registration Date</th>
                        <th>Expire Date</th>
                        <th>History</th>
                        <th>Status</th>
                        <th>Action</th>                       
                    </tr>
    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)){ ?>
                    <tr>
                        <td><?php echo $row['registration_number']; ?></td>
                        <td><?php echo $row['type']; ?></td>
                        <td><?php echo $row['category']; ?></td>
                        <td><?php echo $row['registration_date']; ?></td>
                        <td><?php echo $row['expire_date']; ?></td>
                        <td><ul><li>Engine Changed</li><li>Minor Accident</li></ul></td>
                        <td><?php if($row['status']==0) echo "Pending";else echo "Approved"; ?></td>
                        <td>
                        <?php if($row['status']==0) { ?>
                         <a href="index.php?approve=<?php echo $row['id']; ?>" class="btn btn-success">Approve</a> <br/><br/>
                            <?php } ?>
                        
                        <a href="add-history.php?registration_number=<?php echo $row['registration_number']; ?>" class="btn btn-warning">Add History</a>
                        </td>
                    </tr>
                <?php } ?>
                </table>
            </div>
        </div>
    
    
    </div>
      <script src="js/jquery-1.12.3.js"></script>   
      <script src="js/bootstrap.min.js"></script>   
</body>
</html>