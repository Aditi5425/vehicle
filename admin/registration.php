<?php include("header.php"); ?>
<?php
function AddDays($current_date,$days_to_add)
{
    $date = new DateTime($current_date);
    $days = "P".$days_to_add."D";
    $date->add(new DateInterval($days));

    return $date->format('Y-m-d');
}

if(isset($_POST['registration_number']))
{   
    // Taking Login Form Data
    $registration_number = $_POST['registration_number'];
    $type = $_POST['type'];
    $category = $_POST['category'];
    $registration_date = $_POST['registration_date'];
    $validation_period = $_POST['validation_period'];
    $owner_id = $_POST['owner_id'];
    
    $expire_date = AddDays($registration_date,365*$validation_period);
    
    
    // Connecting Database
    $mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);
    
    // Running Query
    $result = $mysqli->query("INSERT INTO registration_info VALUES(null,$registration_number,'$type','$category','$registration_date','$expire_date',$owner_id)");
    
    
    $success = "Successfully Registered";
    
}
?>
        <div class="row">
            <h2>New Registration</h2>
            <?php if(isset($success)){ ?>
            <label class="label label-success"><?php echo $success; ?></label>
            <?php } ?>
            <div class="col-sm-6">
              <form method="post">
              <div class="form-group">
                <label for="exampleInputEmail1">Registration Number</label>
                <input name="registration_number" type="number" class="form-control"  placeholder="Registration Number">
              </div>
              <div class="form-group">
                <label>Type</label>
                <select name="type" class="form-control">
                    <option value="light">Light</option>  
                    <option value="medium">Medium</option>  
                    <option value="heavy">Heavy</option>  
                </select>
              </div>
              <div class="form-group">
                <label>Category</label>
                <select name="category" class="form-control">
                    <option value="bus">Bus</option>  
                    <option value="truck">Truck</option>  
                    <option value="container">Container</option>  
                    <option value="car">Car</option>  
                </select>
              </div>
            <div class="form-group">
                <label>Registration Date</label>
                <input name="registration_date" type="text" class="form-control"  placeholder="Registration Date">
            </div>
            <div class="form-group">
                <label>Validation Period</label>
                <select name="validation_period" class="form-control">
                    <option value="5">5 Years</option>  
                    <option value="10">10 Years</option>  
                    <option value="3">3 Years</option>  
                    <option value="15">15 Years</option>  
                </select>
            </div>
             <div class="form-group">
                <label for="exampleInputEmail1">Owner ID</label>
                <input name="owner_id" type="number" class="form-control"  placeholder="Owner ID">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
            </form> 
            </div>
        </div>
    
    
    </div>
      <script src="js/jquery-1.12.3.js"></script>   
      <script src="js/bootstrap.min.js"></script>   
</body>
</html>