<?php include("header.php"); ?>
<?php
function AddDays($current_date,$days_to_add)
{
    $date = new DateTime($current_date);
    $days = "P".$days_to_add."D";
    $date->add(new DateInterval($days));

    return $date->format('Y-m-d');
}

$id = $_GET['id'];

if(isset($_POST['license_number']))
{   
    // Taking Login Form Data
    $license_number = $_POST['license_number'];
    $email = $_POST['email'];
    $type = $_POST['type'];
    $full_name = $_POST['full_name'];
    $dob = $_POST['dob'];
    $status = $_POST['status'];
    $exam_date = $_POST['exam_date'];
    $national_id = $_POST['national_id'];
    $registration_date = $_POST['registration_date'];
    $validation_period = $_POST['validation_period'];
    
    if($validation_period ==0)
        $expire_date = '';
    else
    $expire_date = AddDays($registration_date,365*$validation_period);
    
    
    $photo = basename($_FILES["photo"]["name"]);    $target_file = "upload/". $photo; 
     
    // Connecting Database
    $mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);
    
    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file))
    {

        // Running Query
        $result = $mysqli->query("UPDATE  driving_license
        SET license_number='$license_number',name='$full_name',dob='$dob',national_id='$national_id',type='$type',
        registration_date='$registration_date',   expire_date='$expire_date',
        photo='$photo',
        status='$status',
        exam_date='$exam_date',
        validation_period='$validation_period' 
        WHERE id=$id");
        $success = "Updated Successfully!"; 
        
        if($status=="Not Appeared")
        {
           $subject = "Your Driving Exam Date Assigned";
            $message = 'Your Driving Exam date is '.$exam_date;
           mail($email, $subject, $message);
        }
    }
    else
    {
         // Running Query
        $result = $mysqli->query("UPDATE  driving_license
        SET license_number='$license_number',name='$full_name',dob='$dob',national_id='$national_id',type='$type',
        registration_date='$registration_date',   expire_date='$expire_date',
        status='$status',
        exam_date='$exam_date',
        validation_period='$validation_period' 
        WHERE id=$id");
        $success = "Updated Successfully!";  
    }
    
}

    // Connecting Database
    $mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

    // Running Query
    $result = $mysqli->query("SELECT * FROM driving_license WHERE id=$id");
    $row = $result->fetch_array(MYSQLI_ASSOC)
?>
           
        <div class="row">
            <h2>Edit Driving License</h2>
            <?php if(isset($success)){ ?>
            <label class="label-success"><?php echo $success; ?></label>
            <?php } ?>
            <div class="col-sm-12">
            <form method="post" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleInputEmail1">License Number</label>
            <input value="<?php echo $row['license_number']; ?>" name="license_number" type="text" class="form-control" id="exampleInputEmail1" placeholder="License Number">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Full Name</label>
            <input value="<?php echo $row['name']; ?>" name="full_name" type="text" class="form-control" id="exampleInputPassword1" placeholder="Full Name">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"> Date of Birth</label>
            <input value="<?php echo $row['dob']; ?>" name="dob" type="text" class="form-control" id="exampleInputPassword1" placeholder="Ex: YYYY-MM-DD">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Exam Date</label>
            <input value="<?php echo $row['exam_date']; ?>" name="exam_date" type="text" class="form-control" id="exampleInputPassword1" placeholder="Ex: YYYY-MM-DD">
          </div>
            <div class="form-group">
            <label for="exampleInputPassword1">National ID</label>
            <input value="<?php echo $row['national_id']; ?>" name="national_id" type="text" class="form-control" id="exampleInputPassword1" placeholder="National ID">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Registration Date</label>
            <input value="<?php echo $row['registration_date']; ?>" name="registration_date" type="text" class="form-control" id="exampleInputPassword1" placeholder="Ex: YYYY-MM-DD">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Photo</label>
            <input name="photo" type="file" id="exampleInputFile">
              <img height="50" width="50" src="upload/driver/<?php echo $row['photo'];?>" />
          </div>
          <div class="form-group">
                <label>Validation Period</label>
                <select name="validation_period" class="form-control">
                    <option <?php if($row['validation_period']==0) echo "selected='selected'"; ?>  value="0">0 Years</option>
                    <option <?php if($row['validation_period']==5) echo "selected='selected'"; ?>  value="5">5 Years</option>  
                    <option <?php if($row['validation_period']==10) echo "selected='selected'"; ?>  value="10">10 Years</option>  
                    <option <?php if($row['validation_period']==3) echo "selected='selected'"; ?> value="3">3 Years</option>  
                    <option <?php if($row['validation_period']==15) echo "selected='selected'"; ?> value="15">15 Years</option>  
                </select>
          </div>
          <div class="form-group">
                <label>Type</label>
                <select name="type" class="form-control">
                    <option <?php if($row['type']=='light') echo "selected='selected'"; ?> value="light">Light</option>  
                    <option <?php if($row['type']=='medium') echo "selected='selected'"; ?> value="medium">Medium</option>  
                    <option <?php if($row['type']=='heavy') echo "selected='selected'"; ?> value="heavy">Heavy</option>  
                </select>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                    <option <?php if($row['status']=='New') echo "selected='selected'"; ?> value="New">New</option>
                    <option <?php if($row['status']=='Not Appeared') echo "selected='selected'"; ?> value="Not Appeared">Not Appeared</option>
                    <option <?php if($row['status']=='Passed') echo "selected='selected'"; ?> value="Passed">Passed</option>
                    <option <?php if($row['status']=='Failed') echo "selected='selected'"; ?> value="Failed">Failed</option>
                </select>
            </div>
                <input type="hidden" value="<?php echo $row['email']; ?>" name="email" />
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
            </div>
        </div>
    

    </div>
      <script src="js/jquery-1.12.3.js"></script>   
      <script src="js/bootstrap.min.js"></script>   
</body>
</html>