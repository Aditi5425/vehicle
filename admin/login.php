<?php
require_once("config/db.php");
 
if(isset($_POST['username']))
{   
    // Taking Login Form Data
    $username = $_POST['username'];
    $p = sha1($_POST['password']);
    
    // Connecting Database
    $mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);
    
    // Running Query
    $result = $mysqli->query("SELECT * FROM users where username='$username' and password='$p'"); 
    
    // Counting Result
    $found = $result->num_rows;
     
    // If results found logged in successfully
    if($found>0)
    {
        // Starting session
        session_start();
        $_SESSION['admin'] = 1; // Assigning a value for future                   
        header("Location: index.php"); // Return to the homepage
    }
    else
        die("Sorry! Invalid username or password.");  // Login failed 
}
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Bootstrap</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
    <div class="container">
        
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 login_box">
                <h2>Admin Login</h2>
            <form method="post" action="">
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input name="username" type="text" class="form-control" id="exampleInputEmail1" placeholder="Username">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
              </div>

              <button type="submit" class="btn btn-primary">Login</button>
            </form>
            
            </div>
           
        
        </div>
    
    
    </div>
      <script src="js/jquery-1.12.3.js"></script>   
      <script src="js/bootstrap.min.js"></script>   
</body>
</html>