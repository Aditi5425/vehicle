<?php include("header.php"); ?>
<?php
function AddDays($current_date,$days_to_add)
{
    $date = new DateTime($current_date);
    $days = "P".$days_to_add."D";
    $date->add(new DateInterval($days));

    return $date->format('Y-m-d');
}

// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_POST['title']))
{   
    // Taking Login Form Data
    $title = $_POST['title'];
    $type = $_POST['type'];
    $description = $_POST['description'];
    $price = $_POST['price'];
    $price_type = $_POST['price_type'];
    $location = $_POST['location'];
    $phone = $_POST['phone'];
    $mileage = $_POST['mileage'];
    $owner_id = $_SESSION['owner_id'];
    
    $photo = basename($_FILES["photo"]["name"]);      
    $target_file = "admin/upload/ads/". $photo; 
    
              
    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file))
    {
    
    // Running Query
    $query = "INSERT INTO ads VALUES(null,'$title','$description','$photo','$type',$price,'$price_type',$mileage,'$location','$phone',$owner_id)";
    $result = $mysqli->query($query);
    $success = "Your Ads has been created successfully.";
    //echo $query; die();    
    }
    else
        $error = "Sorry! There was an error.";   
}

?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
				<div class="col span_2_of_3">
				  <div class="contact-form">
                      <?php if(isset($success)){ ?>
                      <label class="text-success">
                      <?php echo $success; ?>
                      </label>
                      <?php } ?>
				  	<h3>Create New Ads</h3>
				<form method="post" action="" enctype="multipart/form-data">
                    <div class="row">
					   <div class="col-sm-6">
						    	<span><label>Ads Title</label></span>
						    	<span><input name="title" type="text" class="textbox" required></span>
				        </div>
				        <div class="col-sm-6">
						    	<span><label>Type</label></span>
						    	<span>
                                <select name="type">
                    <option value="heavy">Heavy</option>
                    <option value="medium">medium</option>
                    <option value="heavy">Light</option>
                                </select>
                                </span>
						  </div>
                         <div class="col-sm-12">
						    	<span><label>Description</label></span>
						    	<span>
                                <textarea name="description" required></textarea>
                                    
                                </span>
						  </div>
						  <div class="col-sm-6">
						     	<span><label>Photo</label></span>
						    	<span><input name="photo" type="file" class="textbox" required></span>
						    </div>
                        <div class="col-sm-6">
						     	<span><label>Price</label></span>
						    	<span>
                                    <input type="number" class="textbox" name="price" required />
                                    </span>
						  </div>
                         <div class="col-sm-6">
						    	<span><label>Price Type</label></span>
						    	<span>
                                <select name="price_type">
                    <option value="Fixed">Fixed</option>
                    <option value="Negotiable">Negotiable</option>
                                </select>
                                </span>
						  </div>
                        <div class="col-sm-6">
						    	<span><label>Location</label></span>
						    	<span>
                    <select name="location">
            <option value="Dhaka">Dhaka</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Rajshahi">Rajshahi</option>
            <option value="Khulna">Khulna</option>
            <option value="Sylhet">Sylhet</option>
            <option value="Rangpur">Rangpur</option>
            <option value="Barisal">Barisal</option>
                                </select>
                                </span>
						  </div>
                         <div class="col-sm-6">
						     	<span><label>Mileage</label></span>
						    	<span>
                                    <input type="number" class="textbox" name="mileage" />
                                    </span>
						  </div>
                         <div class="col-sm-6">
						     	<span><label>Contact Number</label></span>
						    	<span>
                                    <input type="text" class="textbox" name="phone" required />
                                    </span>
						  </div>
                            </div>
                            <div class="col-sm-3 ">
						   <div>
						   		<span><input type="submit" value="Create"></span>
						  </div>
                            </div>
					    </form>

				    </div>
  				</div>	
			  </div>
			</div>
			</div>
		 	<!---End-contact----->
		 	<div class="bottom-grids">
		 		<div class="wrap">
		 		<div class="social-links">
		 			<ul>
		 				<li><a href="#">Facebook</a></li>
		 				<li><a href="#">Twitter</a></li>
		 				<li><a href="#">Google+</a></li>
		 				<li><a href="#">Linkedin</a></li>
		 			</ul>
		 		</div>
		 		<div class="mobile-no">
		 			 <span>+91 123 456789</span>
		 		</div>
		 		<div class="footer-logo">
		 			<a href="#">CITY TAXI</a>
		 		</div>
		 		<div class="copy-right">
		 			<p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		 		</div>
		 		<div class="top-to-page">
						<a href="#top" class="scroll"> </a>
						<div class="clear"> </div>
					</div>
		 	</div>
		 	</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
          $("#category").change(function(){
             var img =  "images/"+$("#category").val()+".jpg";
              $("#vehicle_image").attr("src",img);
              
          });
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

