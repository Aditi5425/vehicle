<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_POST['full_name']))
{
    $full_name = $_POST['full_name'];
    $type = $_POST['type'];
    $email = $_POST['email'];
    $dob = $_POST['dob'];
    $nid = $_POST['nid'];
    
    $query_check = "SELECT * FROM driving_license WHERE national_id='$nid'";
    $result_check = $mysqli->query($query_check);
    if($result_check->num_rows<1)
    {
    
    $photo = basename($_FILES["photo"]["name"]);      
    $target_file = "admin/upload/driver/". $photo; 
    
              
    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file))
    {
        
    $query = "INSERT INTO driving_license VALUES(null,'$full_name','$dob','$nid','','$type','','','$photo','','','New','$email')";
        
    $mysqli->query($query);
    $success = "Your Applicatin submitted! You will be notified soon about your exam date.";
    }
    else
        $error = "Sorry! There is an error with upload.";
    }
    else
    {
        $row = $result_check->fetch_array(MYSQLI_ASSOC);
        $current_status = $row['status'];
        $error = "Sorry! You already Applied for driving license. The status is ".$current_status;
    }  
}

?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
				<div class="col span_2_of_3">
				  <div class="contact-form">
                      <?php if(isset($success)){ ?>
                      <label class="text-success">
                      <?php echo $success; ?>
                      </label>
                      <?php } ?>
                       <?php if(isset($error)){ ?>
                      <label class="text-danger">
                      <?php echo $error; ?>
                      </label>
                      <?php } ?>
				  	<h3>Apply For Driving Test</h3>
				<form method="post" action="" enctype="multipart/form-data">
                    <div class="row">
					   <div class="col-sm-6">
						    	<span><label>Full Name</label></span>
						    	<span><input name="full_name" type="text" class="textbox" required></span>
				        </div>
				        <div class="col-sm-6">
						    	<span><label>Email</label></span>
						    	<span><input name="email" type="email" class="textbox" required></span>
						  </div>
						  <div class="col-sm-6">
						     	<span><label>Date of Birth</label></span>
						    	<span><input name="dob" type="text" class="textbox" required></span>
						    </div>
                        <div class="col-sm-6">
						     	<span><label>National ID</label></span>
						    	<span><input name="nid" type="text" class="textbox" required></span>
						  </div>
                         <div class="col-sm-6">
						     	<span><label>Type</label></span>
						    	<span> 
                                    <select name="type" class="textbox">
                                    <option value="light">Light</option>  
                                    <option value="medium">Medium</option>  
                                    <option value="heavy">Heavy</option>  
                                </select>
                             </span>
						  </div>
                         <div class="col-sm-6">
						     	<span><label>Photo</label></span>
						    	<span><input name="photo" type="file" class="textbox" required></span>
						  </div>
                            </div>
                            <div class="col-sm-3 ">
						   <div>
						   		<span><input type="submit" value="Apply Now"></span>
						  </div>
                            </div>
					    </form>

				    </div>
  				</div>	
			  </div>
			</div>
			</div>
		 	<!---End-contact----->
		 	<div class="bottom-grids">
		 		<div class="wrap">
		 		<div class="social-links">
		 			<ul>
		 				<li><a href="#">Facebook</a></li>
		 				<li><a href="#">Twitter</a></li>
		 				<li><a href="#">Google+</a></li>
		 				<li><a href="#">Linkedin</a></li>
		 			</ul>
		 		</div>
		 		<div class="mobile-no">
		 			 <span>+91 123 456789</span>
		 		</div>
		 		<div class="footer-logo">
		 			<a href="#">CITY TAXI</a>
		 		</div>
		 		<div class="copy-right">
		 			<p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		 		</div>
		 		<div class="top-to-page">
						<a href="#top" class="scroll"> </a>
						<div class="clear"> </div>
					</div>
		 	</div>
		 	</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

