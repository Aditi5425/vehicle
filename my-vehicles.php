<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_POST['search_by']))
{
    if($_POST['search_by']=='registration_number')
    {
        $val = $_POST['registration_number'];
        $query = "SELECT * FROM registration_info WHERE registration_number=$val";
    }
    else if($_POST['search_by']=='registration_date')
    {
        $val = $_POST['registration_date'];
        $query = "SELECT * FROM registration_info WHERE registration_date='$val'";
    }
    else if($_POST['search_by']=='owner_id')
    {
        $val = $_POST['owner_id'];
        $query = "SELECT * FROM registration_info WHERE owner_id=$val";
    }
    else
    {
        $error = "Please select something";
    }
    
    if(!isset($error))
    $result = $mysqli->query($query);
       
}

$owner_id = $_SESSION['owner_id'];
$query = "SELECT * FROM registration_info WHERE owner_id=$owner_id";
$result = $mysqli->query($query);

$query_expire = "SELECT * FROM registration_info WHERE owner_id=$owner_id and (expire_date - DATE(NOW())) < 10";
$result_expire = $mysqli->query($query_expire);
?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
                    <?php if(isset($result)){ ?>
                    <div class="col span_3_of_3">
                    
                        <?php if($result_expire->num_rows>0) { ?>
                        <marquee>
                        <?php while($r1 = $result_expire->fetch_array(MYSQLI_ASSOC)){ ?>
                        <label class="text-danger">
                        The vehicle with registration id# <?php echo $r1['registration_number']; ?> will expire soon.</label> 
                        <?php } ?>
                            </marquee>
                        <?php } ?>
                        
                    <h2>My Vehicles</h2>
                    <a href="vehicle-registration.php" class="btn btn-primary">Apply For New Registration</a>
                    <table class="table table-bordered">
                    <tr><th>Registration Number</th><th>Status</th><th>Type</th><th>Category</th><th>Registration Date</th><th>Expire Date</th>
                    <th>History</th>    
                        </tr>
                    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)){ ?>    
                    <tr>
                        <td><?php echo $row['registration_number']; ?></td>
                        <td><?php if($row['status']==0) echo "Pending"; else echo "Approved"; ?></td>
                        <td><?php echo $row['type']; ?></td>
                        <td><?php echo $row['category']; ?></td>
                        <td><?php echo $row['registration_date']; ?></td>
                        <td><?php echo $row['expire_date']; ?></td>
                        <td><a  href="history-details.php?registration_number=<?php echo $row['registration_number']; ?>">View Details</a></td>
                    </tr>
                        <?php } ?>
                    </table>
                    
                </div>
                <?php } ?>
			  </div>
			</div>
			</div>
		 	<!---End-contact----->
		 	<div class="bottom-grids">
		 		<div class="wrap">
		 		<div class="social-links">
		 			<ul>
		 				<li><a href="#">Facebook</a></li>
		 				<li><a href="#">Twitter</a></li>
		 				<li><a href="#">Google+</a></li>
		 				<li><a href="#">Linkedin</a></li>
		 			</ul>
		 		</div>
		 		<div class="mobile-no">
		 			 <span>+91 123 456789</span>
		 		</div>
		 		<div class="footer-logo">
		 			<a href="#">CITY TAXI</a>
		 		</div>
		 		<div class="copy-right">
		 			<p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		 		</div>
		 		<div class="top-to-page">
						<a href="#top" class="scroll"> </a>
						<div class="clear"> </div>
					</div>
		 	</div>
		 	</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

