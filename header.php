<?php 
session_start();
include("admin/config/db.php");
?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
	<head>
		<title>Vehicle Central System</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"  media="all" />
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"  media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css"  media="all" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		  <script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>
	</head>
	<body>
		<!---strat-wrap----->
		
			<!---strat-header----->
			<div class="header"  id="top">
				<div class="wrap">
					<!---start-logo---->
					<div class="logo">
						<a href="index.php"><img src="images/logo1.png" title="logo" /></a>
					</div>
					<!---End-logo---->
					<!---start-top-nav---->
					<div class="top-nav">
						<ul>
                            <?php if(isset($_SESSION['owner_id'])&&$_SESSION['owner_id']!=0){
                            ?>
                            <li><a href="my-vehicles.php">My Vehicles</a></li>
                            <li><a href="my-ads.php">My Ads</a></li>
                             <li><a href="my-account.php">My Account</a></li>
                            <li><a href="ads.php"> Buy/Sell</a></li>
                             <li><a href="logout.php">Logout</a></li>
                            <?php }else{ ?>
							<li><a href="index.php"> Home</a></li>
                            <li><a href="ads.php"> Buy/Sell</a></li>
							<li><a href="owner-signup.php"> Signup</a></li>
							<li><a href="login.php">Login</a></li>
							<li><a href="driving-test.php">Driving Test</a></li>
                            <?php } ?>
							<div class="clear"> </div>
						</ul>
					</div>
					<div class="clear"> </div>
					<!---End-top-nav---->
				</div>
			</div>
			<!---End-header----->

