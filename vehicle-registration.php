<?php include("header.php"); ?>
<?php
function AddDays($current_date,$days_to_add)
{
    $date = new DateTime($current_date);
    $days = "P".$days_to_add."D";
    $date->add(new DateInterval($days));

    return $date->format('Y-m-d');
}

// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_POST['registration_number']))
{   
    // Taking Login Form Data
    $registration_number = $_POST['registration_number'];
    $type = $_POST['type'];
    $category = $_POST['category'];
    $registration_date = $_POST['registration_date'];
    $validation_period = $_POST['validation_period'];
    $owner_id = $_SESSION['owner_id'];
    
    $expire_date = AddDays($registration_date,365*$validation_period);
    
    
    // Connecting Database
    $mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);
    
    // Running Query
    $result = $mysqli->query("INSERT INTO registration_info VALUES(null,$registration_number,'$type','$category','$registration_date','$expire_date',$owner_id,0)");
    
    
    $success = "Your Registraion request has been sent.";
    
}

?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
				<div class="col span_2_of_3">
				  <div class="contact-form">
                      <?php if(isset($success)){ ?>
                      <label class="text-success">
                      <?php echo $success; ?>
                      </label>
                      <?php } ?>
				  	<h3>New Vehicle Registration</h3>
				<form method="post" action="">
                    <div class="row">
					   <div class="col-sm-6">
						    	<span><label>Registration Number</label></span>
						    	<span><input name="registration_number" type="text" class="textbox" required></span>
				        </div>
				        <div class="col-sm-6">
						    	<span><label>Type</label></span>
						    	<span>
                                <select name="type">
                    <option value="heavy">Heavy</option>
                    <option value="medium">medium</option>
                    <option value="heavy">Light</option>
                                </select>
                                </span>
						  </div>
                         <div class="col-sm-6">
						    	<span><label>Category</label></span>
						    	<span>
                                <select id="category" name="category">
                    <option value="bus">Bus</option>
                    <option value="truck">Truck</option>
                    <option value="car">Car</option>
                    <option value="microbus">Microbus</option>
                    <option value="taxi">Taxi</option>
                                </select>
                                <img id="vehicle_image" height="100" width="200" src="images/bus.jpg" />    
                                </span>
						  </div>
						  <div class="col-sm-6">
						     	<span><label>Registration Date</label></span>
						    	<span><input name="registration_date" type="text" class="textbox" required></span>
						    </div>
                        <div class="col-sm-6">
						     	<span><label>Validation Period</label></span>
						    	<span><select name="validation_period">
                    <option value="5">5 Years</option>  
                    <option value="10">10 Years</option>  
                    <option value="3">3 Years</option>  
                    <option value="15">15 Years</option>  
                </select></span>
						    </div>
                            </div>
                            <div class="col-sm-3 ">
						   <div>
						   		<span><input type="submit" value="Register"></span>
						  </div>
                            </div>
					    </form>

				    </div>
  				</div>	
			  </div>
			</div>
			</div>
		 	<!---End-contact----->
		 	<div class="bottom-grids">
		 		<div class="wrap">
		 		<div class="social-links">
		 			<ul>
		 				<li><a href="#">Facebook</a></li>
		 				<li><a href="#">Twitter</a></li>
		 				<li><a href="#">Google+</a></li>
		 				<li><a href="#">Linkedin</a></li>
		 			</ul>
		 		</div>
		 		<div class="mobile-no">
		 			 <span>+91 123 456789</span>
		 		</div>
		 		<div class="footer-logo">
		 			<a href="#">CITY TAXI</a>
		 		</div>
		 		<div class="copy-right">
		 			<p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		 		</div>
		 		<div class="top-to-page">
						<a href="#top" class="scroll"> </a>
						<div class="clear"> </div>
					</div>
		 	</div>
		 	</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
          $("#category").change(function(){
             var img =  "images/"+$("#category").val()+".jpg";
              $("#vehicle_image").attr("src",img);
              
          });
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

