<?php include("header.php"); ?>
<?php 
// Connecting Database
$mysqli = new mysqli($sql_login_host, $sql_login_user, $sql_login_pass, $sql_login_db);

if(isset($_POST['email']))
{
    $email = $_POST['email'];
    $password = sha1($_POST['password']);
    
    $query = "SELECT * FROM vehicle_owner WHERE email='$email' and password='$password'";
    
    $result = $mysqli->query($query);
    $found = $result->num_rows;
    
    
    if($found>0)
    {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        session_start();
        $_SESSION['owner_id'] = $row['id'];
        $_SESSION['owner_email'] = $row['email'];
        $_SESSION['owner_phone'] = $row['phone'];
        $_SESSION['owner_name'] = $row['name'];
        header("Location: my-vehicles.php");
    }
    else
    {
        $error = "Invalid email or password";
    }
    
}

?>
		 <!---start-contnet---->
		 <div class="content">
		 	<!---start-contact----->
		 	<div class="contact">
		 		<div class="wrap">
				<div class="section group">				
				<div class="col span_2_of_3">
				  <div class="contact-form">
                      <?php if(isset($error)){ ?>
                      <label class="text-danger">
                      <?php echo $error; ?>
                      </label>
                      <?php } ?>
				  	<h3>Login As Owner</h3>
				<form method="post" action="">
                    <div class="row">
				        <div class="col-sm-6">
						    	<span><label>Email</label></span>
						    	<span><input value="<?php if(isset($email)) echo $email; ?>" name="email" type="email" class="textbox" required></span>
						  </div>
						  <div class="col-sm-6">
						     	<span><label>Password</label></span>
						    	<span><input name="password" type="password" class="textbox" required></span>
						    </div>
                            </div>
                            <div class="col-sm-3 ">
						   <div>
						   		<span><input type="submit" value="Login"></span>
						  </div>
                            </div>
					    </form>

				    </div>
  				</div>	
			  </div>
			</div>
			</div>
		 	<!---End-contact----->
		 	<div class="bottom-grids">
		 		<div class="wrap">
		 		<div class="social-links">
		 			<ul>
		 				<li><a href="#">Facebook</a></li>
		 				<li><a href="#">Twitter</a></li>
		 				<li><a href="#">Google+</a></li>
		 				<li><a href="#">Linkedin</a></li>
		 			</ul>
		 		</div>
		 		<div class="mobile-no">
		 			 <span>+91 123 456789</span>
		 		</div>
		 		<div class="footer-logo">
		 			<a href="#">CITY TAXI</a>
		 		</div>
		 		<div class="copy-right">
		 			<p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		 		</div>
		 		<div class="top-to-page">
						<a href="#top" class="scroll"> </a>
						<div class="clear"> </div>
					</div>
		 	</div>
		 	</div>
		 </div>
		 <!---End-contnet---->
		</div>
		<!---End-wrap----->

     <script>
      $(document).ready(function(){
        $('.hidden_normally').hide(); 
          
        $("#search_by").change(function(){
           var vl = $(this).val();
            if(vl=='registration_date')
            {
                $("#registration_date").slideDown(1000);
                $("#registration_number").hide();
                $("#owner_id").hide();
                
            }
            else if(vl=='registration_number')
            {
                $("#registration_date").hide();
                $("#registration_number").slideDown(1000);
                $("#owner_id").hide();    
            }
            else if(vl=='owner_id')
            {
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").slideDown(1000);    
            }
            else{
                $("#registration_date").hide();
                $("#registration_number").hide();
                $("#owner_id").hide();   
            }
            
        });  
      });
    </script>
	</body>
</html>

