-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2016 at 07:33 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vehicledb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `photo` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `price_type` varchar(50) NOT NULL,
  `mileage` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `driving_license`
--

CREATE TABLE IF NOT EXISTS `driving_license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `national_id` varchar(50) NOT NULL,
  `license_number` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `registration_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `photo` varchar(100) NOT NULL,
  `validation_period` int(11) NOT NULL,
  `exam_date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `driving_license`
--

INSERT INTO `driving_license` (`id`, `name`, `dob`, `national_id`, `license_number`, `type`, `registration_date`, `expire_date`, `photo`, `validation_period`, `exam_date`, `status`, `email`) VALUES
(1, 'Someone edited again', '1990-01-08', '33333', '44445555', 'light', '2016-05-06', '2021-05-05', '14.jpg', 5, '0000-00-00', '', ''),
(2, 'New Name', '1995-05-04', '9999999', '88888', 'medium', '2018-05-06', '2023-05-05', 'ap2.png', 3, '0000-00-00', '', ''),
(3, 'Mr. Driver', '2016-08-15', '11154544545', '', 'light', '0000-00-00', '0000-00-00', 'logo.png', 0, '2016-05-04', 'Not Appeared', 'info@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_number` double NOT NULL,
  `history_date` date NOT NULL,
  `history_details` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `registration_number`, `history_date`, `history_details`) VALUES
(3, 555, '2015-05-01', 'This is new history added for test'),
(4, 555, '2017-08-02', 'This is latest');

-- --------------------------------------------------------

--
-- Table structure for table `registration_info`
--

CREATE TABLE IF NOT EXISTS `registration_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_number` double NOT NULL,
  `type` varchar(30) NOT NULL,
  `category` varchar(30) NOT NULL,
  `registration_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `owner_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `registration_info`
--

INSERT INTO `registration_info` (`id`, `registration_number`, `type`, `category`, `registration_date`, `expire_date`, `owner_id`, `status`) VALUES
(1, 110022, 'heavy', 'bus', '2016-09-08', '2020-09-08', 0, 1),
(2, 111155, 'medium', 'container', '0000-00-00', '2016-08-31', 1, 1),
(3, 122155, 'medium', 'container', '0000-00-00', '2016-09-09', 1, 1),
(4, 114155, 'medium', 'container', '0000-00-00', '2016-09-07', 1, 1),
(5, 411155, 'medium', 'container', '2016-05-06', '2021-05-05', 0, 0),
(6, 555, 'heavy', 'bus', '2018-05-06', '2033-05-02', 1, 0),
(7, 888888, 'heavy', 'container', '2016-05-06', '2021-05-05', 1, 0),
(8, 333333, 'medium', 'truck', '2016-05-06', '2026-05-04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'info@admin.com');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_owner`
--

CREATE TABLE IF NOT EXISTS `vehicle_owner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `vehicle_owner`
--

INSERT INTO `vehicle_owner` (`id`, `name`, `email`, `password`, `phone`) VALUES
(1, 'John Doe edited', 'info@yahoo.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '00005555');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
